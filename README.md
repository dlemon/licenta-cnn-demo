README

What is this repository for?

	* train-app
		- caffe + keras implementation + CNN weights
		- simple + deep CNNs

	* web-app
		- demo for activity detection
		- client: upload a video and send to server
		- server: process video and detection action -> send response to client


How do I get set up?

	* train-app
		- python "script_name".py
	* web-app
		* client
			- npm install
			- bower install
			- grunt dev & grunt watch
		
		* server
			- npm install
			- python index.js
