import os
from data_module import *


def avi_to_mp4(dir_name):
    for file in os.listdir(dir_name):
        command = 'ffmpeg -i ' + './' + dir_name + '/' + file + ' ' + './mp4/' + file[:-4] + '.mp4'
        os.system(command)


def compute_frames(dir_name, frames_dir, num_frames = 50):
    for file in os.listdir(dir_name):
        file_path = './' + dir_name +'/' + file
	frames_path = frames_dir + file[:-4] + '/'
        print file_path
	print frames_path
        compute_rgb_frames(file_path, frames_path, num_frames)


if __name__ == "__main__":

    compute_frames("mp4", "./test_rgb/");
