import keras
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential, Model
from keras.layers import Dropout, Flatten, Dense
import matplotlib.pyplot as plt

top_model_weights_path = 'fc_model.h5'
# dimensions of our images.
img_width, img_height = 267, 200#320, 240

train_data_dir = 'new_images/train'
validation_data_dir = 'new_images/test'
nb_train_samples = 8 * 512
nb_validation_samples = 128
epochs = 20
batch_size = 16

# build th network ResNet50
network_model = "ResNet50"
nr_non_trainable_layers = 50
base_model = applications.ResNet50(weights='imagenet', include_top=False, input_shape=(img_height, img_width , 3))
print('Model loaded.')

last = base_model.output

print base_model.output_shape[1:]

x = Flatten(input_shape=base_model.output_shape[1:])(last)
x = Dense(256, activation='relu')(x)
x = Dropout(0.5)(x)
x = Dense(10, activation='sigmoid')(x)


model = Model(base_model.input, x)


print "IESEIRE", model.output_shape

for layer in model.layers[:nr_non_trainable_layers]
    layer.trainable = False

# compile the model with a SGD/momentum optimizer
# and a very slow learning rate.
model.compile(loss='categorical_crossentropy',
              optimizer=optimizers.SGD(lr=0.0001, momentum=0.9),
              metrics=['accuracy'])

# prepare data augmentation configuration
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='categorical')#'binary')

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='categorical')#'binary')

# fine-tune the model
history = model.fit_generator(
    train_generator,
    samples_per_epoch=nb_train_samples,
    epochs=epochs,
    validation_data=validation_generator,
    nb_val_samples=nb_validation_samples)

model.save_weights("../fine-tuning-new-" + network_model + ".h5")

print(history.history.keys())
#  "Accuracy"
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
#plt.show()

plt.savefig("finetuning-new" + network_model + ".jpg")
