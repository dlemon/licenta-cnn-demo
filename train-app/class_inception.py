import keras
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential, Model
from keras.layers import Dropout, Flatten, Dense
from keras.models import load_model
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np

import matplotlib.pyplot as plt

predict_data_dir = 'images_predict'
batch_size = 50#5


img_width, img_height = 267, 200
model_weights = "cnn_weights/fine-tuning-InceptionV3.h5"

base_model = applications.InceptionV3(weights='imagenet', include_top=False, input_shape=(img_height, img_width , 3))
print('Model loaded.')
last = base_model.output
print base_model.output_shape[1:]
x = Flatten(input_shape=base_model.output_shape[1:])(last)
x = Dense(256, activation='relu')(x)
x = Dropout(0.5)(x)
x = Dense(10, activation='sigmoid')(x)
model = Model(base_model.input, x)

model.load_weights(model_weights, by_name=False)

predict_datagen = ImageDataGenerator(rescale=1. / 255)
predict_generator = predict_datagen.flow_from_directory(
    predict_data_dir,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='sparse')

predict = model.predict_generator(predict_generator,1)
#print('Predicted:', predict)


labels = ["BenchPress",
"Biking",
"Drumming",
"GolfSwing",
"PlayingGuitar",
"PlayingPiano",
"PullUps",
"PushUps",
"TennisSwing",
"WalkingWithDog"
]

for x in predict.tolist():
    argmax = np.argmax(x, axis=0)
    print argmax, labels[argmax]
