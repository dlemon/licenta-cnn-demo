''' Axinte Dana Raluca, code for DATA MODULE'''

import subprocess
import os
from argparse import ArgumentParser


''' Get video duration of filename '''
def compute_video_length(video_file):
    comm = 'ffprobe -i ' + video_file + ' -show_entries format=duration -v quiet -of csv="p=0"'
    proc = subprocess.Popen([comm], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    res = float(out)
    return res


''' Split video in num_frames  '''
def split_video_frames(video_file, video_name, video_fps, frames_dir_name, num_frames):

    #frames_dir_name = "./images_predict/" + video_name

    if not os.path.exists(frames_dir_name):
        os.makedirs(frames_dir_name)
        out_file = frames_dir_name + "/output_" + video_name + '_%02d.jpg'
        command = 'ffmpeg -i ' + video_file + ' -r ' + str(video_fps) + ' -f image2 ' + out_file
        os.system(command)

        video_frames = []
        for file in os.listdir(frames_dir_name):
            video_frames.append(file)
        for i in range(num_frames):
            f_name = frames_dir_name + '/' + video_frames[i] +'[267x200]'
            resize_command = 'convert ' + f_name + " "+ frames_dir_name + '/resized_'+ video_frames[i]
            os.system(resize_command)

        for i in range(len(video_frames)):
            os.remove(frames_dir_name + '/' + video_frames[i])
        print "[%s] Method: %s, Success: %s." % ("data_module", "split_video_frames", "Created RGB frames")

    else:
        print "[%s] Method: %s, Error: %s." % ("data_module", "split_video_frames", "file folder already exists")


''' Compute RGB frames of a video '''
def compute_rgb_frames(video_file, frames_folder, num_frames = 50):

    video_length = compute_video_length(video_file)
    video_fps = 1.0 / (video_length/num_frames)

    video_name = video_file[video_file.rfind('/')+1:-4]
    frames_dir_name = frames_folder  + video_name

    split_video_frames(video_file, video_name, video_fps, frames_dir_name, num_frames)

    return frames_dir_name


''' Compute Optical Flow of a video '''
def compute_optical_flow(video_file, of_folder):

    video_name = video_file[video_file.rfind('/')+1:-4]
    of_dir_name_u = of_folder  + '/u/' + video_name
    of_dir_name_v = of_folder  + '/v/' + video_name

    return (of_dir_name_u, of_dir_name_v)


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("--video_name", type = str, default = '',
                        help="Video Name")

    parser.add_argument("--frames_dir", type = str, default = '',
                        help="Frames dir")

    parser.add_argument("--num_frames", type = int, default = '',
                        help="Number of frames")

    args = parser.parse_args()
    
    compute_rgb_frames(args.video_name, args.frames_dir, args.num_frames)
    #compute_optical_flow('./v_Biking_g24_c02.avi', '/media/dana/New Volume1/Work/FACULTATE/LICENTA/tvl1_flow')