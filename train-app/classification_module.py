''' Axinte Dana Raluca, code for CLASSIFICATION '''

from collections import Counter
import numpy as np

labels = ["BenchPress",
"Biking",
"Drumming",
"GolfSwing",
"PlayingGuitar",
"PlayingPiano",
"PullUps",
"PushUps",
"TennisSwing",
"WalkingWithDog"
]

''' CNNs fusion and final prediction '''
def fusion_cnns(predict_rgb, predict_of):
    global labels


    results1 = []
    results2 = []

    for x in predict_rgb.tolist():
        argmax = np.argmax(x, axis=0)
        results1.append(argmax)

    for x in predict_of.tolist():
        argmax = np.argmax(x, axis=0)
        results2.append(argmax)
    res = results1 + results2

    ct = Counter(res)
    max_label = ct.most_common()[0][0]

    return(results1, results2, max_label)
    