import data_module
import activity_recognition_module
import classification_module
import shutil
from argparse import ArgumentParser


labels = ["BenchPress",
"Biking",
"Drumming",
"GolfSwing",
"PlayingGuitar",
"PlayingPiano",
"PullUps",
"PushUps",
"TennisSwing",
"WalkingWithDog"
]


''' Video processing by the system '''

def process_video(video_file, frames_dir, of_dir):

    rgb_dir = data_module.compute_rgb_frames(video_file, frames_dir)
    (of_dir_u, of_dir_v) = data_module.compute_optical_flow(video_file, frames_dir)

    batch_size = 50
    img_width, img_height = 267, 200

    model_rgb = activity_recognition_module.create_module_resnet50()
    pred_rgb = activity_recognition_module.predict_cnn(model_rgb, frames_dir, img_width, img_height)

    model_of = activity_recognition_module.create_module_caffenet()
    pred_of = activity_recognition_module.predict_cnn(model_of, frames_dir, img_width, img_height)


    (res_rgb, resof, max_class) = classification_module.fusion_cnns(pred_rgb, pred_of)
    print labels[max_class]


def predict_objects(rgb_dir, cnn_dir):

    img_width, img_height = 267, 200
    video_frames = []

    for file in os.listdir(rgb_dir):
        video_frames.append(file)

    activity_recognition_module.predict_imagenet(cnn_dir, video_frames, img_width, img_height)


if __name__ == "__main__":


    parser = ArgumentParser()
    parser.add_argument("--video_name", type = str, default = '',
                        help="Video Name")

    parser.add_argument("--frames_dir", type = str, default = '',
                        help="Frames dir")

    args = parser.parse_args()


    #video_file = 'v_Biking_g24_c02.mp4'
    video_file = args.video_name
    frames_dir = args.frames_dir
    process_video(video_file, frames_dir, frames_dir)



