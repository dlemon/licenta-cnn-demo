''' Axinte Dana Raluca, code for ACTIVITY RECOGNITION MODULE '''

import keras
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential, Model
from keras.layers import Dropout, Flatten, Dense
from keras.models import load_model
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np



''' RESNET50 CNN initialization '''
def create_module_resnet50():

    batch_size = 50#5
    img_width, img_height = 267, 200
    model_weights = "cnn_weights/fine-tuning-new-newResNet50.h5"

    base_model = applications.ResNet50(weights='imagenet', include_top=False, input_shape=(img_height, img_width , 3))
    last = base_model.output
    x = Flatten(input_shape=base_model.output_shape[1:])(last)
    x = Dense(256, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(10, activation='sigmoid')(x)
    model = Model(base_model.input, x)
    model.load_weights(model_weights, by_name=False)

    print "[%s] Method: %s, Success: %s." % ("data_module", "create_module_resnet50", "model loaded")
    return model


''' CAFFENET CNN initialization '''
def create_module_caffenet():

    batch_size = 50#5
    img_width, img_height = 267, 200
    model_weights = "cnn_weights/fine-tuning-InceptionV3.h5"

    base_model = applications.InceptionV3(weights='imagenet', include_top=False, input_shape=(img_height, img_width , 3))
    print('Model loaded.')
    last = base_model.output
    x = Flatten(input_shape=base_model.output_shape[1:])(last)
    x = Dense(256, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(10, activation='sigmoid')(x)
    model = Model(base_model.input, x)

    print "[%s] Method: %s, Success: %s." % ("data_module", "create_module_caffenet", "model loaded")
    return model


''' CNN prediction '''
def predict_cnn(model, predict_data_dir, img_width, img_height, batch_size = 50):

    predict_datagen = ImageDataGenerator(rescale=1. / 255)
    predict_generator = predict_datagen.flow_from_directory(
                            predict_data_dir,
                            target_size=(img_height, img_width),
                            batch_size=batch_size,
                            class_mode='sparse')
    predict = model.predict_generator(predict_generator,1)

    return predict


''' CNN object detection '''
def predict_imagenet(image_path, images_list, img_width, img_height):
    model = applications.ResNet50(weights='imagenet')
    #img_path = 'elephant.jpg'

    res_pred = []
    for i in range(len(images_list)):
        img = image.load_img(images_list[i], target_size=(img_width, img_height))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)

        preds = model.predict(x)
        # decode the results into a list of tuples (class, description, probability)
        # (one such list for each sample in the batch)
        print('Predicted:', decode_predictions(preds, top=5)[0])
        res_pred.append(preds)

    return res_pred


if __name__ == "__main__":

    #predict_data_dir = 'images_predict'
    #batch_size = 50
    img_width, img_height = 267, 200

    #model = create_module_resnet50(predict_data_dir)
    #pred = predict_cnn(model, predict_data_dir, img_width, img_height)

    predict_imagenet()


